import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ShowplanComponent } from './showplan/showplan.component';
import { BavtvComponent } from './bavtv/bavtv.component';
import { InfoComponent } from './info/info.component';
import { DatenschutzComponent } from './datenschutz/datenschutz.component';
import { ImpressumComponent } from './impressum/impressum.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'showplan',
    component: ShowplanComponent
  },
  {
    path: 'bavtv',
    component: BavtvComponent
  },
  {
    path: 'info',
    component: InfoComponent
  },
  {
    path: 'datenschutz',
    component: DatenschutzComponent
  },
  {
    path: 'impressum',
    component: ImpressumComponent
  },
  {
    path: '**',
    redirectTo: '/'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
