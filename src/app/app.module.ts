import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { FilterShowArrayPipe } from './pipes/filterShowArray.pipe';
import { HttpsPipe } from './pipes/https.pipe';

import { ImgFallbackModule } from 'ngx-img-fallback';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { HomeComponent } from './home/home.component';
import { ShowplanComponent } from './showplan/showplan.component';
import { BavtvComponent } from './bavtv/bavtv.component';
import { InfoComponent } from './info/info.component';
import { PlayerComponent } from './player/player.component';
import { AktuelleShowComponent } from './home/aktuelle-show/aktuelle-show.component';
import { NextShowsComponent } from './home/next-shows/next-shows.component';
import { ShowComponent } from './showplan/show/show.component';
import { DayComponent } from './showplan/day/day.component';
import { DatenschutzComponent } from './datenschutz/datenschutz.component';
import { ImpressumComponent } from './impressum/impressum.component';
import { FreundeComponent } from './freunde/freunde.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    HomeComponent,
    ShowplanComponent,
    BavtvComponent,
    InfoComponent,
    PlayerComponent,
    AktuelleShowComponent,
    NextShowsComponent,
    ShowComponent,
    DayComponent,
    FilterShowArrayPipe,
    HttpsPipe,
    DatenschutzComponent,
    ImpressumComponent,
    FreundeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ImgFallbackModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
