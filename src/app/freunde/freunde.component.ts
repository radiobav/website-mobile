import { Component, OnInit } from '@angular/core';
import { FreundeService } from '../services/freunde.service';

@Component({
  selector: 'app-freunde',
  templateUrl: './freunde.component.html',
  styleUrls: ['./freunde.component.css']
})
export class FreundeComponent implements OnInit {

  freunde: Array<string>

  constructor(
    private freundeService: FreundeService
  ) { }

  getFreundeImage (): void {
    this.freundeService.getImagesUrl()
      .then(res => this.freunde = res)
  }

  ngOnInit() {
    this.freunde = undefined;
    this.getFreundeImage();
  }

}
