import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AktuelleShowComponent } from './aktuelle-show.component';

describe('AktuelleShowComponent', () => {
  let component: AktuelleShowComponent;
  let fixture: ComponentFixture<AktuelleShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AktuelleShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AktuelleShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
