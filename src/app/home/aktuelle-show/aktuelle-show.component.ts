import { Component, OnInit, Input } from '@angular/core';
import { Show } from 'src/app/interfaces/show';

@Component({
  selector: 'app-aktuelle-show',
  templateUrl: './aktuelle-show.component.html',
  styleUrls: ['./aktuelle-show.component.css']
})
export class AktuelleShowComponent implements OnInit {

  @Input('show') show:Show;

  constructor() { }

  ngOnInit() {
  }

}
