import { Component, OnInit } from '@angular/core';
import { ShowsServiceService } from '../services/shows-service.service';
import { Show } from '../interfaces/show';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  currentShow: Show;
  nextShows: Show[];

  constructor(
    private showService: ShowsServiceService
  ) { }

  getUpcommingShows(): void {
    this.showService.getUpcommingShows(4)
      .then( res => {
          this.nextShows = res;
      })
  }

  getCurrentShow(): void {
    this.showService.getCurrentShow()
      .then(res => {
        this.currentShow = res;
      })
  }

  ngOnInit() {
    this.currentShow = undefined;
    this.nextShows = undefined;
    this.getCurrentShow();
    this.getUpcommingShows();
  }

}
