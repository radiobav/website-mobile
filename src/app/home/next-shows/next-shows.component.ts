import { Component, OnInit, Input } from '@angular/core';
import { Show } from 'src/app/interfaces/show';


@Component({
  selector: 'app-next-shows',
  templateUrl: './next-shows.component.html',
  styleUrls: ['./next-shows.component.css']
})
export class NextShowsComponent implements OnInit {

  constructor() { }

  @Input('shows') shows: Show[];

  ngOnInit() {
  }

}
