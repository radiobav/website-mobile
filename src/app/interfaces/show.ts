export interface Show {
    name: string;
    description: string;
    id: number;
    instance_id: number;
    url: string;
    image_path: string;
    starts: string;
    ends: string;
}