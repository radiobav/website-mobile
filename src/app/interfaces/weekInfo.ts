import { Show } from "./show";

export interface WeekInfo {
    monday: Show[];
    tuesday: Show[];
    wednesday: Show[];
    thursday: Show[];
    friday: Show[];
    nextmonday: Show[];
    nexttuesday: Show[];
    nextwednesday: Show[];
    nextthursday: Show[];
    netxfriday: Show[];
}