import { Pipe, PipeTransform } from '@angular/core';
import { Show } from '../interfaces/show';

@Pipe({name: 'filterShows'})
export class FilterShowArrayPipe implements PipeTransform {
    transform(arr: Show[]) {
        return this.filterDuplicates(arr);
    }

    filterDuplicates(arr: Show[]) {

        let newShows: Show[] = [];

        for (let i = 0; i < arr.length; i++) {
            
            const show = arr[i];

            if (newShows.length < 1 || newShows[newShows.length -1].name.trim().toUpperCase() != show.name.trim().toUpperCase()) {
                newShows.push(show);
            } else {
                newShows[newShows.length -1].ends = show.ends;
            }
            
        }

        return newShows;


    }
}