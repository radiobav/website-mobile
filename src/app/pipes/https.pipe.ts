import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'https'})
export class HttpsPipe implements PipeTransform {
    transform(s: string): string {
        return s.replace(/^http:\/\//i, 'https://');
    }
}