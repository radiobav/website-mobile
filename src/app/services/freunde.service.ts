import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FreundeService {

  constructor(
    private http:HttpClient
  ) { }

  getImagesUrl(): Promise<string[]> {
    return this.http.get<any>('https://m.radiobav.de/api/freunde')
    .toPromise()
    .then( (res) => {
      if (res) {
        return res;
      }
      return [];
    })
    .catch(res => console.log('Error fetching slider images:' + res));
  }
}
