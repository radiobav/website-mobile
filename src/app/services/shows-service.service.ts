import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Show } from '../interfaces/show';
import { WeekInfo } from '../interfaces/weekInfo';


@Injectable({
  providedIn: 'root'
})
export class ShowsServiceService {

  constructor(
    private http: HttpClient
  ) { }

  getUpcommingShows (amount: number): Promise<Show[]> {
    const url = `https://airtime.radiobav.de/api/live-info-v2?shows=${amount + 1}`;

    return this.http.get<any>(url)
    .toPromise()
    .then(res => {
      if(res && res.shows && res.shows.next) {
        return res.shows.next as Show[];
      }
    })
    .catch((res) => {
      console.log(`Error: Http GET ${url}`);
      return [] as Show[];
    })
  }

  getCurrentShow (): Promise<Show>| undefined {
    const url = `https://airtime.radiobav.de/api/live-info-v2?shows=1`;

    return this.http.get<any>(url)
    .toPromise()
    .then(res => {
      if(res && res.shows && res.shows.current) {
        return res.shows.current as Show;
      }
    })
    .catch((res) => {
      console.log(`Error: Http GET ${url}`);
      return undefined;
    })
  }

  getSchedule (): Promise<WeekInfo> | undefined {
    const url = 'https://airtime.radiobav.de/api/week-info';

    return this.http.get<any>(url)
    .toPromise()
    .then(res => {
      if (res) {
        return res as WeekInfo;
      }
    })
    .catch( res => {
      console.log(`Error: Http GET ${url}`);
      return undefined;
    })
  }

}
