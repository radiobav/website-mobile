import { Component, OnInit, Input } from '@angular/core';
import { Show } from 'src/app/interfaces/show';

@Component({
  selector: 'app-day',
  templateUrl: './day.component.html',
  styleUrls: ['./day.component.css']
})
export class DayComponent implements OnInit {

  @Input('shows') shows: Show[];
  date: Date;
  
  constructor() { }

  Date (date: string) {
    return new Date(date);
  }

  ngOnInit() {
    this.date = new Date();
  }

}
