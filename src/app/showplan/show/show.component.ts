import { Component, OnInit, Input } from '@angular/core';
import { Show } from 'src/app/interfaces/show';

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {

  @Input('show') show: Show

  constructor() { }

  ngOnInit() {
  }

}
