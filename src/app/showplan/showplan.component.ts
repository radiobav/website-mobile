import { Component, OnInit } from '@angular/core';
import { WeekInfo } from '../interfaces/weekInfo';
import { ShowsServiceService } from '../services/shows-service.service';

@Component({
  selector: 'app-showplan',
  templateUrl: './showplan.component.html',
  styleUrls: ['./showplan.component.css']
})
export class ShowplanComponent implements OnInit {

  scheduel: WeekInfo;

  constructor(
    private showService: ShowsServiceService
  ) { }

  getWeekInfo () {
    this.showService.getSchedule()
      .then( res => {
        this.scheduel = res;
      })
  }

  ngOnInit() {
    this.scheduel = undefined;
    this.getWeekInfo();
  }

}
